import 'package:get/get.dart';
class Dimensions{
  //current height is 780
  static double screenHeight = Get.context!.height;
  static double screenWidth = Get.context!.width;

  static double pageView = screenHeight/2.43; //780/320
  static double pageViewContainer = screenHeight/3.54; // là phần của slide chia tỉ lệ - lấy 780 (chiều cao của thiết bị) / 220 (chiều cao theo height của container) = 3.84
  static double pageViewTextContainer = screenHeight/6.5; //780/120

  //dynamic height padding and margin
  static double height10 = screenHeight/78; // 780/10
  static double height15 = screenHeight/52; // 780/15
  static double height20 = screenHeight/39;
  static double height30 = screenHeight/26;
  static double height45 = screenHeight/17.33;

  //dynamic width padding and margin
  static double width10 = screenHeight/78;
  static double width15 = screenHeight/52;
  static double width20 = screenHeight/39;
  static double width30 = screenHeight/26;
  static double width45 = screenHeight/17.33;

  //font size
  static double font16 = screenHeight/48.75;
  static double font20 = screenHeight/45;
  static double font26 = screenHeight/30;

  //radius
  static double radius15 = screenHeight/52;
  static double radius20 = screenHeight/39;
  static double radius30 = screenHeight/26;

  //icon Size
  static double iconSize16 = screenHeight/48.75;
  static double iconSize24 = screenHeight/35.17;

  static double listViewImgSize = screenHeight/6.5;
  static double listViewTextContSize = screenHeight/7.8;

  //popular food
  static double popularFoodImgSize = screenHeight/2.22;

  //bottom height
static double bottomHeihgtBar = screenHeight/6.5;
}